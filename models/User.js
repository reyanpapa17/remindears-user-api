import { Schema, model } from "mongoose";

const UserSchema = Schema({
  email: {
    type: String,
    required: true,
    min: 8,
    max: 30,
  },
  password: {
    type: String,
    required: true,
    min: 8,
    max: 1024,
  },
  firstName: {
    type: String,
    required: true,
    min: 1,
    max: 30,
  },
  middleName: {
    type: String,
    required: true,
    min: 1,
    max: 30,
  },
  lastName: {
    type: String,
    required: true,
    min: 1,
    max: 30,
  },
  addresses: {
    type: [String],
    required: true,
  },
  age: {
    type: Number,
    required: true,
  },
  birthday: {
    month: {
      type: Number,
      required: true,
    },
    day: {
      type: Number,
      required: true,
    },
    year: {
      type: Number,
      required: true,
    },
  },
  gender: {
    type: String,
    required: true,
  },
  maritalStatus: {
    type: String,
    required: true,
  },
  mobile: {
    areaCode: {
      type: String,
      required: true,
    },
    number: {
      type: String,
      required: true,
    },
  },
  roleName: {
    type: String,
    required: true,
  },
  updatedDate: {
    type: String,
    required: false
  }
});

export default model("User", UserSchema);
