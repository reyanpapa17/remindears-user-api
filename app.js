import express from "express";
import { connect } from "mongoose";
import cors from "cors";
import { json } from "body-parser";
import "dotenv/config";
// Import Routes
import postsRoute from "./routes/posts";
import userRoute from "./routes/user";

const app = express();

app.use(cors());
app.use(json());

app.use("/posts", postsRoute);
app.use("/user", userRoute);

//Routes
app.get("/", (req, res) => {
  res.send("We are on HomePage");
});

// Connect MongoDB
connect(
  process.env.DB_CONNECTION,
  { useUnifiedTopology: true, useNewUrlParser: true },
  () => {
    console.log("Connected to DB");
  }
);

app.listen(3000, () => {
  console.log("Running RESTful API on port http://localhost:3000");
});
