const Joi = require('joi');

const registerValidation = data => {
	const RegistrationSchema = Joi.object({
		email: Joi.string().required().email(),
		password: Joi.string().min(8).required(),
		firstName: Joi.string().required(),
		middleName: Joi.string().required(),
		lastName: Joi.string().required(),
		addresses: Joi.array().min(1).items(Joi.string()),
		age: Joi.number().required(),
		birthday: Joi.object().keys({
			month: Joi.number().max(12).required(),
			day: Joi.number().max(31).required(),
			year: Joi.number().required()
		}),
		gender: Joi.string().max(6).required(),
		maritalStatus: Joi.string().required(),
		mobile: Joi.object().keys({
			areaCode: Joi.string().required(),
			number: Joi.string().required()
		}),
		roleName: Joi.string().required()
	});

	return RegistrationSchema.validate(data);
};

const loginValidation = data => {
	const LoginSchema = Joi.object({
		email: Joi.string().min(6).required().email(),
		password: Joi.string().min(8).required()
	});

	return LoginSchema.validate(data);
};

export { loginValidation, registerValidation };
