import { Schema, model } from "mongoose";

const PostSchema = Schema({
  userID: {
    type: String,
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

export default model("Posts", PostSchema);
