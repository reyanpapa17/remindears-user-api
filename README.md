# REST-API
This API has CRUD operation for creating user and generate specific post for that user

Nodemon
- add nodemon.json in root folder and include the setup below:
```
{
    "events": {
        "restart": "kill-port 3000",
        "crash": "kill-port 3000"
    },
    "delay": "1500"
}
```
- execute "npm install --global kill-port"